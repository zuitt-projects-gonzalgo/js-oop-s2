// let studentTwoName = "Joe";
// let studentTwoEmail = "joe@mail.com";
// let studentTwoGrades = [78, 82, 79, 85];

// let studentThreeName = "Jane";
// let studentThreeEmail = "jane@mail.com";
// let studentThreeGrades = [87, 89, 91, 93];

// let studentFourName = "Jessie";
// let studentFourEmail = "jessie@mail.com";
// let studentFourGrades = [91, 89, 92, 93];

// A C T I V I T Y

let studentOne = {
	name: "John",
	email: "john@mail.com",
	grades: [89, 84, 78, 88],
	login(){
		console.log(`${this.email} has logged in`)
	},
	logout(){
		console.log(`${this.email} has logged out`)
	},
	listGrades(){
		console.log(`${this.name}'s quarterly averages are: ${this.grades}`)
	},
	computeAve() {
		let sum = 0;
		for(let i=0; i < this.grades.length; i++) {
			sum += this.grades[i]
		}
		
		let avg = sum/this.grades.length
		return avg;
	},
	willPass() {
		if(this.computeAve() >= 85){
			return true;
		} else {
			return false;
		}
	},
	willPassWithHonors() {
		if(this.computeAve() >= 90) {
			return true;
		} else if(this.computeAve() >= 85 && this.computeAve() < 90) {
			return false;
		} else {
			return undefined;
		}
	}
}

let studentTwo = {
	name: "Joe",
	email: "joe@mail.com",
	grades: [78, 82, 79, 85],
	login(){
		console.log(`${this.email} has logged in`)
	},
	logout(){
		console.log(`${this.email} has logged out`)
	},
	listGrades(){
		console.log(`${this.name}'s quarterly averages are: ${this.grades}`)
	},
	computeAve() {
		let sum = 0;
		for(let i=0; i < this.grades.length; i++) {
			sum += this.grades[i]
		}
		
		let avg = sum/this.grades.length
		return avg;
	},
	willPass() {
		if(this.computeAve() >= 85){
			return true;
		} else {
			return false;
		}
	},
	willPassWithHonors() {
		if(this.computeAve() >= 90) {
			return true;
		} else if(this.computeAve() >= 85 && this.computeAve() < 90) {
			return false;
		} else {
			return undefined;
		}
	}
}

let studentThree = {
	name: "Jane",
	email: "jane@mail.com",
	grades: [87, 89, 91, 93],
	login(){
		console.log(`${this.email} has logged in`)
	},
	logout(){
		console.log(`${this.email} has logged out`)
	},
	listGrades(){
		console.log(`${this.name}'s quarterly averages are: ${this.grades}`)
	},
	computeAve() {
		let sum = 0;
		for(let i=0; i < this.grades.length; i++) {
			sum += this.grades[i]
		}
		
		let avg = sum/this.grades.length
		return avg;
	},
	willPass() {
		if(this.computeAve() >= 85){
			return true;
		} else {
			return false;
		}
	},
	willPassWithHonors() {
		if(this.computeAve() >= 90) {
			return true;
		} else if(this.computeAve() >= 85 && this.computeAve() < 90) {
			return false;
		} else {
			return undefined;
		}
	}
}

let studentFour = {
	name: "Jessie",
	email: "jessie@mail.com",
	grades: [91, 89, 92, 93],
	login(){
		console.log(`${this.email} has logged in`)
	},
	logout(){
		console.log(`${this.email} has logged out`)
	},
	listGrades(){
		console.log(`${this.name}'s quarterly averages are: ${this.grades}`)
	},
	computeAve() {
		let sum = 0;
		for(let i=0; i < this.grades.length; i++) {
			sum += this.grades[i]
		}
		
		let avg = sum/this.grades.length
		return avg;
	},
	willPass() {
		if(this.computeAve() >= 85){
			return true;
		} else {
			return false;
		}
	},
	willPassWithHonors() {
		if(this.computeAve() >= 90) {
			return true;
		} else if(this.computeAve() >= 85 && this.computeAve() < 90) {
			return false;
		} else {
			return undefined;
		}
	}
}

//Translate the other students from our boilerplate code into their own respective objects.
//DONE


//Define a method for EACH student object that will compute for their grade average (total of grades divided by 4)
//DONE


//Define a method for all student objects named willPass() that returns a Boolean value indicating if student will pass or fail. For a student to pass, their ave. grade must be greater than or equal to 85.
//DONE


//Define a method for all student objects named willPassWithHonors() that returns true if ave. grade is greater than or equal to 90, false if >= 85 but < 90, and undefined if < 85 (since student will not pass).
//DONE


//Create an object named classOf1A with a property named students which is an array containing all 4 student objects in it.

let classOf1A = {
	students: [studentOne, studentTwo, studentThree, studentFour],
	countHonorStudents() {
		let result = 0;
		this.students.forEach(student => {
			if(student.willPassWithHonors()) {
				result++
			}
		})
		return result;
	},
	honorsPercentage() {
		let result = (this.countHonorStudents() / this.students.length) * 100 + "%";
		return result;
	},
	retrieveHonorStudentInfo() {
		let honorStudents = [];
		this.students.forEach(student => {
			if(student.willPassWithHonors()) {
				honorStudents.push({
					email: studen.email,
					aveGrade: student.computeAve()
				})
			}
		})
		return honorStudents;
	},
	sortHonorStudentsByGradeDesc() {
		return this.retrieveHonorStudentInfo().sort((studenA, studentB) => studentB.aveGrade - studentA.aveGrade)
	}
}


//Create a method for the object classOf1A named countHonorStudents() that will return the number of honor students.



//Create a method for the object classOf1A named honorsPercentage() that will return the % of honor students from the batch's total number of students.
//


//Create a method for the object classOf1A named retrieveHonorStudentInfo() that will return all honor students' emails and ave. grades as an array of objects.
//


//Create a method for the object classOf1A named sortHonorStudentsByGradeDesc() that will return all honor students' emails and ave. grades as an array of objects sorted in descending order based on their grade averages.
//